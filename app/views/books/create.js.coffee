$("#new_row").append "<%= j render 'data_row', book: @book %>"
rows = $("tr[id^=book_]")
if rows.size() >= 10
  rows.first().find("td").wrapInner("<div>").parent().
  find("td > div").slideUp 300, ->
    rows.first().remove()
$(".rateit").rateit()